#include <iostream>
#include "submodule/submodule.h"

#define TEST_VERSION 1

int main() {

    std::cout << "test program version " << TEST_VERSION << std::endl;
    std::cout << "submodule    version " << SUBMODULE_VERSION << std::endl;
    std::cout << "subsubmodule version " << SUBSUBMODULE_VERSION << std::endl;
    std::cout << std::endl;

    return 0;
}
